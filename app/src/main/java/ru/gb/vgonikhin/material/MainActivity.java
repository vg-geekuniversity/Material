package ru.gb.vgonikhin.material;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;

import static ru.gb.vgonikhin.material.Constants.APP_THEME;
import static ru.gb.vgonikhin.material.Constants.SHARED_PREFS;
import static ru.gb.vgonikhin.material.R.id.fragment_container;

public class MainActivity extends AppCompatActivity {

    MainFragment mainFragment;
    ThemeChangerFragment themeChangerFragment;
    SharedPreferences sPref;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sPref = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);

        int themeID = sPref.getInt(APP_THEME, R.style.RedTheme);
        this.setTheme(themeID);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mainFragment = new MainFragment();
        themeChangerFragment = new ThemeChangerFragment();
        replaceFragment(mainFragment);

        initMainUI();
        initNavUI();
    }

    private void replaceFragment(Fragment newFragment) {
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(fragment_container, newFragment);
        transaction.commit();
    }

    private void initNavUI() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(getNavigationListener());
    }

    private NavigationView.OnNavigationItemSelectedListener getNavigationListener() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                switch (id) {
                    case R.id.nav_gallery:
                        replaceFragment(mainFragment);
                        break;
                    case R.id.nav_manage:
                        replaceFragment(themeChangerFragment);
                }

                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        };
    }

    private void initMainUI() {
        setSupportActionBar(toolbar);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
