package ru.gb.vgonikhin.material;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import static ru.gb.vgonikhin.material.Constants.APP_THEME;
import static ru.gb.vgonikhin.material.Constants.SHARED_PREFS;

public class ThemeChangerFragment extends android.support.v4.app.Fragment {

    FragmentActivity activity;
    Button blueButton, redButton, greenButton;
    SharedPreferences sPref;

    public ThemeChangerFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_theme_changer, container, false);
        activity = getActivity();
        if (activity != null) {
            sPref = activity.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        }
        blueButton = view.findViewById(R.id.blue_button);
        redButton = view.findViewById(R.id.red_button);
        greenButton = view.findViewById(R.id.green_button);
        blueButton.setOnClickListener(getListener());
        redButton.setOnClickListener(getListener());
        greenButton.setOnClickListener(getListener());
        return view;
    }

    Button.OnClickListener getListener() {
        return new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.blue_button:
                        changeTheme(R.style.BlueTheme);
                        break;
                    case R.id.red_button:
                        changeTheme(R.style.RedTheme);
                        break;
                    case R.id.green_button:
                        changeTheme(R.style.GreenTheme);
                }

            }
        };
    }

    private void changeTheme(int theme) {
        SharedPreferences.Editor e = sPref.edit();
        e.putInt(APP_THEME, theme);
        e.apply();
        startActivity(new Intent(activity.getApplicationContext(), MainActivity.class));
        activity.finish();
    }
}
