package ru.gb.vgonikhin.material;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

public class MainFragment extends Fragment {

    FloatingActionButton fab;

    public MainFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(getListener());
        return view;
    }

    Button.OnClickListener getListener() {
        return new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, R.string.photo_added_snack_text, Snackbar.LENGTH_LONG).show();
                NavigationView navigationView = Objects.requireNonNull(getActivity()).findViewById(R.id.nav_view);
                navigationView.setItemTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
            }
        };
    }
}
