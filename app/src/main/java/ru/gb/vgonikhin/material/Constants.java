package ru.gb.vgonikhin.material;

class Constants {
    static final String SHARED_PREFS = "SharedPrefs";
    static final String APP_THEME = "appTheme";
}
